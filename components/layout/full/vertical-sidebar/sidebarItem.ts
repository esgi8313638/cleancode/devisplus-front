import {
    ApertureIcon,
    CopyIcon,
    LayoutDashboardIcon, LoginIcon, MoodHappyIcon, TypographyIcon, UserPlusIcon, BuildingStoreIcon, FileInvoiceIcon, FileDollarIcon, UserIcon
} from 'vue-tabler-icons';

export interface menu {
    header?: string;
    title?: string;
    icon?: any;
    to?: string;
    chip?: string;
    chipColor?: string;
    chipVariant?: string;
    chipIcon?: string;
    children?: menu[];
    disabled?: boolean;
    type?: string;
    subCaption?: string;
}

const sidebarItem: menu[] = [
    {
        title: 'Accueil',
        icon: LayoutDashboardIcon,
        to: '/'
    },
    {
        title: 'Clients',
        icon: UserIcon,
        to: '/clients/list'
    },
    {
        title: 'Produits',
        icon: BuildingStoreIcon,
        to: '/products/list'
    },
    {
        title: 'Devis',
        icon: FileInvoiceIcon,
        to: '/devis/list'
    },
    {
        title: 'Factures',
        icon: FileDollarIcon,
        to: '/invoices/list'
    }
];

export default sidebarItem;
