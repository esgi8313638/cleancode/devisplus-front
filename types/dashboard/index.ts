/*Recent Transaction*/
type recentTrans = {
    title: string;
    subtitle: string;
    textcolor: string;
    boldtext: boolean;
    line: boolean;
    link: string;
    url: string;
};

/*product performance*/
type productPerformanceType = {
    id: number;
    name: string;
    post: string;
    pname: string;
    status: string;
    statuscolor: string;
    budget: string;
};

/*Products card types*/
type productsCards = {
    name: string;
    description: string;
    price: number;
    quantity: number;
};

/*Devis type*/
type devisType = {
    id: number;
    name: string;
    products: Array<productsCards>;
    date: string;
};

export type { recentTrans, productPerformanceType, productsCards, devisType }