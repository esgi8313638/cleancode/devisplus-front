import type { ThemeTypes } from '@/types/themeTypes/ThemeType';

const PurpleTheme: ThemeTypes = {
    name: 'PurpleTheme',
    dark: false,
    variables: {
        'border-color': '#eeeeee',
        'carousel-control-size': 10
    },
    colors: {
        primary: '#FF6F00',
        secondary: '#2979FF',
        info: '#00B0FF',
        success: '#00C853',
        accent: '#FFAB40',
        warning: '#FFD740',
        error: '#FF1744',
        muted: '#546E7A',
        lightprimary: '#FFE0B2',
        lightsecondary: '#E3F2FD',
        lightsuccess: '#E8F5E9',
        lighterror: '#FFEBEE',
        lightwarning: '#FFF3E0',
        textPrimary: '#263238',
        textSecondary: '#546E7A',
        borderColor: '#CFD8DC',
        inputBorder: '#000',
        containerBg: '#FFFFFF',
        hoverColor: '#F5F5F5',
        surface: '#FFFFFF',
        'on-surface-variant': '#FFFFFF',
        grey100: '#CFD8DC',
        grey200: '#B0BEC5'
    }
};
export { PurpleTheme};
