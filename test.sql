--
-- Base de données : `devisplus`
--

-- --------------------------------------------------------

INSERT INTO `produit` (`id`, `nom`, `description`, `prix`) VALUES
(1, 'Produit 1', 'Description du produit 1', 150),
(2, 'Produit 2', 'Description du produit 2', 250),
(3, 'Produit 3', 'Description du produit 3', 350),
(4, 'Produit 4', 'Description du produit 4', 450),
(5, 'Produit 5', 'Description du produit 5', 550),
(6, 'Produit 6', 'Description du produit 6', 650),
(7, 'Produit 7', 'Description du produit 7', 750),
(8, 'Produit 8', 'Description du produit 8', 850),
(9, 'Produit 9', 'Description du produit 9', 950),
(10, 'Produit 10', 'Description du produit 10', 1050);

-- --------------------------------------------------------

INSERT INTO `client` (`id`, `nom`, `adresse`, `email`, `telephone`) VALUES
(1, 'Client 1', '1 rue dupont, Grenoble 38000', 'client1@gmail.com', '0101010101'),
(2, 'Client 2', '2 rue dupont, Grenoble 38000', 'client2@gmail.com', '0202020202'),
(3, 'Client 3', '3 rue dupont, Grenoble 38000', 'client3@gmail.com', '0303030303'),
(4, 'Client 4', '4 rue dupont, Grenoble 38000', 'client4@gmail.com', '0404040404'),
(5, 'Client 5', '5 rue dupont, Grenoble 38000', 'client5@gmail.com', '0505050505'),
(6, 'Client 6', '6 rue dupont, Grenoble 38000', 'client6@gmail.com', '0606060606');

-- --------------------------------------------------------

INSERT INTO `devis` (`id`, `client_id`, `date_creation`, `statut`) VALUES
(1, 1, '2023-04-24', 'En attente'),
(2, 1, '2024-03-24', 'En attente'),
(3, 2, '2023-04-11', 'Refusé'),
(4, 2, '2024-04-11', 'Refusé'),
(5, 3, '2023-04-11', 'Accepté'),
(6, 3, '2024-04-17', 'En attente'),
(7, 4, '2023-04-11', 'Accepté'),
(8, 4, '2024-03-17', 'En attente'),
(9, 5, '2023-04-11', 'Accepté'),
(10, 5, '2024-04-17', 'En attente'),
(11, 6, '2023-04-11', 'Accepté'),
(12, 6, '2024-04-17', 'En attente');

-- --------------------------------------------------------

INSERT INTO `detail_devis` (`id`, `devis_id`, `quantite`, `prix_unitaire`) VALUES
(1, 1, 1, NULL),
(2, 2, 1, NULL),
(3, 3, 1, NULL),
(4, 4, 1, NULL),
(5, 5, 1, NULL),
(6, 6, 1, NULL),
(7, 7, 1, NULL),
(8, 8, 1, NULL),
(9, 9, 1, NULL),
(10, 10, 1, NULL),
(11, 11, 1, NULL),
(12, 12, 1, NULL);

-- --------------------------------------------------------

INSERT INTO `detail_devis_produit` (`detail_devis_id`, `produit_id`) VALUES
(1, 1),
(1, 2),
(2, 3),
(2, 4),
(3, 5),
(3, 6),
(4, 7),
(4, 8),
(5, 9),
(5, 10),
(6, 1),
(6, 2),
(7, 3),
(7, 4),
(8, 5),
(8, 6),
(9, 7),
(9, 8),
(10, 9),
(10, 10),
(11, 1),
(11, 2),
(12, 3),
(12, 4);

-- --------------------------------------------------------

INSERT INTO `facture` (`id`, `date_creation`, `paiement_ok`, `client_id`, `devis_id`) VALUES
(1, '2023-04-30', 1, 1, 1),
(2, '2024-04-30', 0, 2, 3),
(3, '2023-04-30', 1, 3, 5),
(4, '2024-04-30', 0, 4, 7),
(5, '2023-04-30', 1, 5, 9),
(6, '2024-04-30', 1, 6, 11);
