import type { recentTrans, productPerformanceType, productsCards } from '@/types/dashboard/index';

/*Basic Table 1*/
const productPerformance: productPerformanceType[] = [
    {
        id: 1,
        name: 'Sunil Joshi',
        post: 'Web Designer',
        pname: 'Elite Admin',
        status: 'Low',
        statuscolor: 'primary',
        budget: '$3.9'
    },
    {
        id: 2,
        name: 'Andrew McDownland',
        post: 'Project Manager',
        pname: 'Real Homes WP Theme',
        status: 'Medium',
        statuscolor: 'secondary',
        budget: '$24.5k'
    },
    {
        id: 3,
        name: 'Christopher Jamil',
        post: 'Project Manager',
        pname: 'MedicalPro WP Theme',
        status: 'High',
        statuscolor: 'error',
        budget: '$12.8k'
    },
    {
        id: 4,
        name: 'Nirav Joshi',
        post: 'Frontend Engineer',
        pname: 'Hosting Press HTML',
        status: 'Critical',
        statuscolor: 'success',
        budget: '$2.4k'
    }

];

/*--Products Cards--*/
const productsCard: productsCards[] = [
    {
        name: 'Produit 1',
        description: 'Description du produit 1',
        price: 10,
        quantity: 1,
    },
    {
        name: 'Produit 2',
        description: 'Description du produit 2',
        price: 20,
        quantity: 2,
    },
    {
        name: 'Produit 3',
        description: 'Description du produit 3',
        price: 30,
        quantity: 3,
    }
];

/*--Devis list--*/
const devisList: string[] = []; // Par exemple, vous pouvez initialiser devisList avec un tableau vide de chaînes de caractères

export { productPerformance, productsCard, devisList };